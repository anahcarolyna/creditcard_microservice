package br.com.creditcard.service;

import br.com.creditcard.clients.CustomerClient;
import br.com.creditcard.exceptions.CreditCardNotFoundException;
import br.com.creditcard.models.CreditCard;
import br.com.creditcard.models.Customer;
import br.com.creditcard.repository.CreditCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CreditCardService {

    @Autowired
    private CreditCardRepository creditCardRepository;

    @Autowired
    private CustomerClient customerClient;


    public CreditCard create(CreditCard creditCard) {
        Customer customer = customerClient.getById(creditCard.getCustomer_id());

        creditCard.setActive(false);

        return creditCardRepository.save(creditCard);
    }

    public CreditCard update(CreditCard creditCard) {
        CreditCard databaseCreditCard = getByNumber(creditCard.getNumber());

        databaseCreditCard.setActive(creditCard.getActive());

        return creditCardRepository.save(databaseCreditCard);
    }

    public CreditCard getByNumber(String number) {
        /* Exemplo em 1 linha
        CreditCard creditCard = creditCardRepository.findByNumber(number)
                .orElseThrow(CreditCardNotFoundException::new);
        */

        // nosso código normal
        Optional<CreditCard> byId = creditCardRepository.findByNumber(number);

        if(!byId.isPresent()) {
            throw new CreditCardNotFoundException();
        }

        return byId.get();
    }

    public CreditCard getById(Long id) {
        Optional<CreditCard> byId = creditCardRepository.findById(id);

        if(!byId.isPresent()) {
            throw new CreditCardNotFoundException();
        }

        return byId.get();
    }
}

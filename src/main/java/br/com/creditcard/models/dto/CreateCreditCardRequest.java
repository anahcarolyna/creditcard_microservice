package br.com.creditcard.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreateCreditCardRequest {
    @Column(unique = true)
    @Size(min = 9, max = 16, message = "O número do cartão deve ter entre 9 a 16 caracteres")
    @NotNull(message = "É obrigatório o preenchimento do número do cartão")
    @JsonProperty("numero")
    private String number;

    @NotNull
    @JsonProperty("clienteId")
    private Long customerId;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

}

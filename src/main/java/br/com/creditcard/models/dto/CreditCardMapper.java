package br.com.creditcard.models.dto;

import br.com.creditcard.models.CreditCard;
import br.com.creditcard.models.Customer;
import org.springframework.stereotype.Component;

@Component
public class CreditCardMapper {
    public CreditCard toCreditCard(CreateCreditCardRequest createCreditCardRequest) {
        CreditCard creditCard = new CreditCard();
        creditCard.setNumber(createCreditCardRequest.getNumber());

        creditCard.setCustomer_id(createCreditCardRequest.getCustomerId());

        return creditCard;
    }

    public CreateCreditCardResponse toCreateCreditCardResponse(CreditCard creditCard) {
        CreateCreditCardResponse createCreditCardResponse = new CreateCreditCardResponse();

        createCreditCardResponse.setId(creditCard.getId());
        createCreditCardResponse.setNumber(creditCard.getNumber());
        createCreditCardResponse.setCustomerId(creditCard.getCustomer_id());
        createCreditCardResponse.setActive(creditCard.getActive());

        return createCreditCardResponse;
    }

    public CreditCard toCreditCard(UpdateCreditCardRequest updateCreditCardRequest) {
        CreditCard creditCard = new CreditCard();

        creditCard.setNumber(updateCreditCardRequest.getNumber());
        creditCard.setActive(updateCreditCardRequest.getActive());

        return creditCard;
    }

    public UpdateCreditCardResponse toUpdateCreditCardResponse(CreditCard creditCard) {
        UpdateCreditCardResponse updateCreditCardResponse = new UpdateCreditCardResponse();

        updateCreditCardResponse.setId(creditCard.getId());
        updateCreditCardResponse.setNumber(creditCard.getNumber());
        updateCreditCardResponse.setCustomerId(creditCard.getCustomer_id());
        updateCreditCardResponse.setActive(creditCard.getActive());

        return updateCreditCardResponse;
    }

    public GetCreditCardResponse toGetCreditCardResponse(CreditCard creditCard) {
        GetCreditCardResponse getCreditCardResponse = new GetCreditCardResponse();

        getCreditCardResponse.setId(creditCard.getId());
        getCreditCardResponse.setNumber(creditCard.getNumber());
        getCreditCardResponse.setCustomerId(creditCard.getCustomer_id());

        return getCreditCardResponse;
    }
}

package br.com.creditcard;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import com.netflix.loadbalancer.RoundRobinRule;
import com.netflix.loadbalancer.WeightedResponseTimeRule;
import org.springframework.context.annotation.Bean;

import java.util.Random;

public class RibbonConfiguration {

    //a partir de agora sempre que ele precisar de uma IRULE é pra ele chamar esse getRule.
    @Bean
    public IRule  getRule(){
        return new RandomRule();
    }
}

package br.com.creditcard.clients;

import br.com.creditcard.models.Customer;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "Customer", configuration = CustomerClientConfiguration.class)
public interface CustomerClient {

    @GetMapping("/cliente/cliente/{id}")
    Customer getById(@PathVariable Long id);
}

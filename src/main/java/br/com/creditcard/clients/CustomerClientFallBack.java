package br.com.creditcard.clients;

import br.com.creditcard.exceptions.CustomerOfflineException;
import br.com.creditcard.models.Customer;

public class CustomerClientFallBack implements CustomerClient {

    @Override
    public Customer getById(Long id) {

        throw new CustomerOfflineException();
    }
}

package br.com.creditcard.clients;

import br.com.creditcard.exceptions.InvalidCustomerException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class CustomerClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404){
            return new InvalidCustomerException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}

package br.com.creditcard.repository;

import br.com.creditcard.models.CreditCard;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CreditCardRepository extends CrudRepository<CreditCard, Long> {
    Optional<CreditCard> findByNumber(String number);
}
